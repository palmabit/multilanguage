<?php

return [
  "default" => "it",
  "session_var" => "lingua_corrente",
  "session_var_admin" => "lingua_corrente_admin",
  "path_base_flags" => '/client/img/flag/',
  "extension_img_bandierine" => 'png'
];