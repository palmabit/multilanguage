<?php namespace Palmabit\Multilanguage\Facades;

use Illuminate\Support\Facades\Facade;

class Urltranslator extends Facade {

    protected static function getFacadeAccessor() { return 'urltranslator'; }
}